import { createItemTicket, createItemTicketDetale } from './functions/function_create.js';
import { dataSeasonTickes } from './fake-data/fake-data.js';
import { getIndex } from './functions/utils/getIndex.js';
import { renderDialog } from './component/render-dialog.js';
import { targetRoutScren } from './functions/utils/targetRoutScren.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const scren = document.getElementById('scren');
const [...routs] = document.getElementsByClassName('rout_item');
const [...header_navs] = document.getElementsByClassName('header_nav');

const options = document.getElementById('options')
const [...options_btn] = document.getElementsByClassName('options_btn')
const list = document.getElementById('list');
const details = document.getElementById('details');

// Рендер панелі керування маштабом.
useScaleControl();

scren.addEventListener('click', ev => {
  if (ev.target.id === 'home_btn') {
		targetRoutScren(routs, 'id', 'scren_main');
		targetRoutScren(header_navs, 'id', 'nav_main');
  }
  
	if (ev.target.dataset.id_detale) {
		renderDetails(dataSeasonTickes, ev.target.dataset.id_detale);
    targetRoutScren(routs, 'id', 'scren_details');
    targetRoutScren(header_navs, 'id', 'nav_details');
	}

	if (ev.target.dataset.id_bay) {
    bay(ev.target.dataset.id_bay);
    targetRoutScren(routs, 'id', 'scren_main');
    targetRoutScren(header_navs, 'id', 'nav_main');
  }
})
// Функція додати в кошик.
function bay(id) {
  const basketState = JSON.parse(localStorage.getItem('basketState'))
  let index = getIndex(dataSeasonTickes,id)
  dataSeasonTickes[index].buy = true;
  localStorage.setItem('basketState', JSON.stringify([{ ...dataSeasonTickes[index] }, ...basketState]));
  renderList(dataSeasonTickes);
  renderDialog('Абонімент успішно додано до кошика');
  setTimeout(() => {
    location.href = './basket.html';
  },2000)
}

const optionsActive = () => {
  options_btn.forEach((e, i) => {
    options_btn[i].classList.remove('_active')
  })
}

options.addEventListener('click', (ev) => {
  if (ev.target.classList.value === 'options_btn') {
    optionsActive()
    ev.target.classList.add('_active')
    if (ev.target.dataset.id) {
      renderList(dataSeasonTickes, +ev.target.dataset.id);
    }else renderList(dataSeasonTickes);
  }
})
// Функція рендер основної сторінки.
function renderList(data,num=0) {
  list.innerHTML = '';
  data.forEach((el, i) => {
    if (num <= i) {
      list.insertAdjacentHTML('beforeend', createItemTicket(el));
    }
  })
  basketCaunt()
}
// Функція рендер сторінки деталі.
function renderDetails(data,id) {
  details.innerHTML = '';
  const el = data[getIndex(data, id)];
  details.insertAdjacentHTML('beforeend', createItemTicketDetale(el));
  basketCaunt()
}

renderList(dataSeasonTickes);