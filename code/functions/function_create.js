import { thousandSeparator } from './utils/thousandSeparator.js';
import { truncate } from './utils/truncate.js';

// Створення контенту сторінка "Головна" => "Слайдер докладніше"
export function createDetails(src) {
	return`
    <img class="details_img" src="${src}" alt="gym">
    <h2 class="details_title">Назва</h2>
    <p class="details_description">Опис: Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem voluptatem, tempore doloribus, quibusdam ipsum nisi vel odio nemo maiores impedit fugiat delectus, fugit vitae maxime officiis. Sapiente non qui numquam?</p>
    <p class="details_conditions">Умови: Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa in reprehenderit eum cupiditate architecto placeat, sapiente numquam perferendis rem laudantium voluptatibus exercitationem labore sed ipsum, doloribus quo. Ex, architecto quisquam.</p>
      `;
}

// Створення контенту сторінка "Придбати абонімент" => "Абонімент"
export function createItemTicket(el) {
	return `
  <li class="list_item ${el.buy && '_hide'}" data-id="${el.id}">
    <div class="item_img div1">
      <img src="${el.img}" alt="${el.title}">
    </div>
    <h2 class="item_title div2">${el.title}</h2>
    <p class="item_description div3">${truncate(el.descript, 60)}</p>
    <p class="item_date div4">${el.date}</p>
    <p class="item_time div5">${el.time}</p>
    <button ${el.buy && "disabled='true'"} class="item_detale-btn div6"  data-id_detale="${
		el.id
	}" type="button">детал.︙</button>
    <p class="item_price div7">${thousandSeparator(el.price)} грн.</p>
    <button ${el.buy && "disabled='true'"} class="item_buy_btn div8" data-id_bay="${
		el.id
	}" type="button">придбати</button>
  </li>`;
}
// Створення контенту сторінка "Придбати абонімент" => "Докладніше про абонімент"
export function createItemTicketDetale(el) {
  const detale = `
  <div class="item ${el.buy && '_hide'}" data-id="${el.id}">
    <div class="item_img">
      <img src="${el.img}" alt="${el.title}">
    </div>
    <p class="item_description">${el.descript}</p>
    <p class="item_time">${el.terms_use}</p>
    <p class="item_price">${thousandSeparator(el.price)} &#8372;</p>
    <button ${el.buy && "disabled='true'"} class="buy_btn" data-id_bay="${el.id}" type="button">придбати</button>
  </div>`;
  const message = `
  <div class="item">
    <h2 class="item_title">Абонемент додано до кошика</h2>
  </div>`

  return el.buy ? message : detale
  
}

// Створення контенту сторінка "Магазин спорт залу" => "Товари"
export function createItemSportingGoods(el) {
	return `
  <li class="list_item" data-id_detale="${el.id}">
    <div class="item_img div1 pointer">
      <img src="${el.img}" alt="${el.title}" data-id_detale="${el.id}">
    </div>
    <h2 class="item_title div2">${el.title}</h2>

    <p class="item_description div3">${truncate(el.descript, 50)}</p>

    <p class="item_price div4">${thousandSeparator(el.price)} грн.</p>

    <button class="item_buy_btn div5" data-id_bay="${
		el.id
	}" type="button">придбати</button>
  </li>
  `;
}
// Створення контенту сторінка "Магазин спорт залу" => "Докладніше про Товар"
export function createItemSportingGoodsDetale(el) {
	return `
  <div class="item" data-id="${el.id}">
    <div class="item_detale_img">
      <img src="${el.img}" alt="${el.title}">
    </div>
    <h2 class="item_title">${el.title}</h2>

    <p class="item_description">${el.descript}</p>

    <p class="item_price">${thousandSeparator(el.price)} грн.</p>

    <button ${el.buy && "disabled='true'"} class="buy_btn" data-id_bay="${
		el.id
	}" type="button">придбати</button>
  </div>
  `;
}

// Створення контенту сторінка "Групові заняття"
export function createItemGroupClasses(el) {
  return `
    <p class="item_time">Час: <span>18:00</span></p>
    <h2 class="item_title">Назва</h2>
    <p class="item_description">Зал</p>
    <p class="item_description">Кількість місць: <span>5</span></p>
    <div class="item_info">
      <h2 class="info_title">Період обраний в календарі.</h2>
      <p class="info">Місяць: <span>${el.month}</span></p>
      <p class="info">Тиждень: <span>${el.week}</span></p>  
    </div>
    <button data-id="save" class="buy_btn" type="button">Записатись</button>`;
}

// Створення контенту сторінка "Кабінет тренера" => "Мій профіль"
export function createCoachProfile(el) {
	return `
    <div class="profile_img" id="profile_img">
      <img src="${el.img}" alt="${el.title}">
      <button class="edit" type="button" id="btn_edit">
        <img src="../image/icons/edit.png" alt="edit">
      </button>
    </div>
    <h2 class="profile_title">${el.title}</h2>

    <a class="contact" href="tel:#">
      <img src="../image/icons/phone.png" alt="phone">
      ${el.contacts.phone}</a>
    <a class="contact"  href="mailto:#">
      <img src="../image//icons/mail.png" alt="mail">
     ${el.contacts.mail}</a>
  
    <p class="profile_text">${el.biography}</p>
    <p class="profile_text">${el.specialization}</p>
    <p class="profile_text">Експертиза</p>
  `;
}

// Створення контенту сторінка "Заняття з тренером"
export function createCoachItem(el) {
	return `
    <li class="coach_list_item grid">
      <img class="grid-item-1" data-detale="${el.id}" src="${el.img}" alt="${el.title}">
  
      <h2 class="grid-item-2 flex">${el.title}</h2>
  
      <p class="grid-item-3 flex">${el.specialization}</p>
  
      <button class="grid-item-4 item-btn flex" data-recommendations="${el.id}" type="button">
        Рекомендації від тренера
      </button>

      <button class="grid-item-5 item-btn flex" data-schedule="${el.id}"
      type="button">
        Розклад занять
      </button>
    </li>
  `;
}
// Створення контенту сторінка "Заняття з тренером" => "Докладніше про тренера"
export function createCoachDetaleItem(el) {
	return `
  <div class="coach_item">
  <img class="" src="${el.img}" alt="${el.title}">

  <h2 class=" flex">${el.title}</h2>

  <p class=" flex">${el.specialization}</p>

  <p>${el.biography}</p>

  <button class="coachs_detale_btn" data-id="submit" type="button">Записатись</button>
</div>
  `;
}

//Створення контенту сторінка "Написати в підтримку"
export function createItemSupport(data,flag) {
  if (!Array.isArray(data) || !data.length) {
		return `<h2 class="item_title">У вас немає звернень</h2>`;
  }
  const elements = []
  data.forEach(el => {
    if (flag) {
      elements.push(`<li class="list_item ${el.input && 'input'}">
          ${el.input ?
            `<p class="icon_inbox _input">
              <img src="../image/icons/replyemail.png" alt="replyemail">
            </p>`:
            `<p class="icon_inbox">
              <img src="../image/icons/inbox.png" alt="replyemail">
            </p>`}
            <h2 class="item_title">${el.title}</h2>
            <p class="item_description">${el.descript}</p>
            <p class="item_date">дата: ${el.date}</p>
             ${el.answer ? `<div class="list_item input">
                <p class="icon_inbox _input">
                  <img src="../image/icons/replyemail.png" alt="replyemail">
                </p>
                <h2 class="item_title">${el.answer.title}</h2>
                <p class="item_description">${el.answer.descript}</p>
                <p class="item_date">дата: ${el.answer.date}</p>
                </div>`:''
              }

          </li>`)
    }else{
      if (!el.input && !el.answer) {
				elements.push(`<li class="list_item ${el.input && 'input'}">
             <p class="icon_inbox">
              <img src="../image/icons/inbox.png" alt="replyemail">
            </p>
            <h2 class="item_title">${el.title}</h2>
            <p class="item_description">${el.descript}</p>
            <p class="item_date">дата: ${el.date}</p>
   
            ${
							el.answer
								? `<div class="list_item input">
                <p class="icon_inbox _input">
                  <img src="../image/icons/replyemail.png" alt="replyemail">
                </p>
                <h2 class="item_title">${el.answer.title}</h2>
                <p class="item_description">${el.answer.descript}</p>
                <p class="item_date">дата: ${el.answer.date}</p>
                </div>`
								: `<button class="icon" type="button">
                    <img data-reply="${el.id}" src="../image/icons/comment.png" alt="replyemail">
                  </button>`
						}
          </li>`);
			}
    }
  });


  return `
    <ul class="list">
      ${elements.length ? elements.join(' ') : '<li class="title">Немає нових повідомлень</li>'}
    </ul>
 `;
}

// Створення контенту сторінка "інфармація про клуби" => "Мої абоніменти"
export function createTicket(el, flag = true) {
  if (!flag && el.active) { return ''}
  if (flag && !el.active) {
		return '';
	}
		return `<li class="tickets_list_item ${el.freeze ? '_freeze' : ''}" data-ticketId="${el.id}">
      <img src="${el.img}" alt="${el.title}">
      <div class="item_info">
        <h3>${el.title}</h3>
        <p>${el.date}</p>
      </div>
      
     ${
				flag
					? `<button class="btn_freeze" data-freeze="${el.id}" type="button" ${
							el.freeze ? 'disabled' : ''
					  }>Заморозити</button>
      <button class="btn_details" data-detale="${el.id}" type="button">︙</button>`
					: ''
			}
    </li>`;
}
// Створення контенту сторінка "інфармація про клуби" => "Мій абонімент"
export function createTicketDetale(el) {
  return `<div class="tickets_detale">
    <img src="${el.img}" alt="${el.title}">
    <p class="detale_id">ID:<span>${el.id}</span></p>
    <h3 class="detale_title">${el.title}</h3>
    <p class="detale_date">${el.date}</p>
  </div>`;
}

// Створення контенту сторінка "кошик" => "Оберати спосіб оплати"
export function createPayService() {
	return `
	  <h2 class="wrapper_title">Оберіть спосіб оплати</h2>
    <div class="pay_list grid">

			<button type="button">
				<button class="pay_item div1" type="button" data-pay_service="../image/icons/atm-card.png">
					<img data-pay_service="../image/icons/atm-card.png" src="../image/icons/atm-card.png" alt="pay">
			</button>

			<button data-pay_service="../image/icons/pay-1.png" class="pay_item div2" type="button">
					<img data-pay_service="../image/icons/pay-1.png" src="../image/icons/pay-1.png" alt="pay">
			</button>

			<button data-pay_service="../image/icons/pay-2.png"  class="pay_item div3" type="button">
					<img data-pay_service="../image/icons/pay-2.png"  src="../image/icons/pay-2.png" alt="pay">
			</button>

			<button data-pay_service="../image/icons/pay-3.png"  class="pay_item div4" type="button">
					<img data-pay_service="../image/icons/pay-3.png" src="../image/icons/pay-3.png" alt="pay">
			</button>

			<button data-pay_service="../image/icons/pay-6.png" class="pay_item div5" type="button">
					<img data-pay_service="../image/icons/pay-6.png" src="../image/icons/pay-6.png" alt="pay">
			</button>

			<button data-pay_service="../image/icons/pay-7.png" class="pay_item div6" type="button">
					<img data-pay_service="../image/icons/pay-7.png" src="../image/icons/pay-7.png" alt="pay">
			</button>

			<button data-pay_service="../image/icons/pay-4.png"  class="pay_item div7" type="button">
					<img data-pay_service="../image/icons/pay-4.png" src="../image/icons/pay-4.png" alt="pay">
			</button>
		</div>
	`;
}
// Створення контенту сторінка "кошик" => "Підтвердження оплати"
export function createConfirmPayment(img, totalPrace) {
	return `
	<div class="box_pay_img">
		<img src="${img}" alt="pay">
	</div>
	<div class="box_pay">
		<p class="total_price" id="total_price">Загальна сума: <span>${thousandSeparator(totalPrace)}</span> грн.</p>
		<div class="check_box_wrapp">
			<input class="checkbox" type="checkbox" required id="checkbox">
			<label for="checkbox">Договір оферти від залу</label>
		</div>

		<button class="btn-submit" type="button" id="btn-submit" disabled >Сплатити</button>
	</div>
	`;
}