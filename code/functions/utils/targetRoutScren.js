// Функція перемикання між елементами по дата атрибуту.
export function targetRoutScren(elements, dataset, dataset_name) {
	elements.forEach(el => {
		el.dataset[dataset] === dataset_name ? el.classList.remove('_hide') : el.classList.add('_hide');
	});
}