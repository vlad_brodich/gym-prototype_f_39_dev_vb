// функція прибирає сласи з елементів
export function removeClass(data) {
	data.forEach(el => {
		el.classList.remove('_active');
		el.classList.remove('_hide');
	});
};
