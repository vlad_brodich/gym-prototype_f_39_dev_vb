 // Функція створення ID або Key.
export function createId(num = 6){
	const letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	let newId = '';

	for (let i = 0; i < num; i++) {
		const randomIndex = Math.floor(Math.random() * letters.length);
		newId += letters.charAt(randomIndex);
	}

	return newId;
}
