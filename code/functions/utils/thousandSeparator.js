// Функція розділяє великі числа на тисячи пробілами та міняє крапку на кому 
export function thousandSeparator(num) {
  const str = String(num.toFixed(2));
	const reg = /\d{1,3}(?=(\d{3})+(?!\d))/g;
	if (str === '') {
		return '0';
	} else return str.replace(reg, '$& ').replace(/\./g, ',');
}