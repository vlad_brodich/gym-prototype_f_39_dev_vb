// Функція повертає індекс елементу в масиві.
export function getIndex(data, id) {
	return data.findIndex(elem => elem.id === id);
}
