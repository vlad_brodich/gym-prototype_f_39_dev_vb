// Функція перемикання між елементами.
export function routerSwitch(routOn, routOff) {
  routOn.classList.remove('_hide');
	routOff.classList.add('_hide');
}