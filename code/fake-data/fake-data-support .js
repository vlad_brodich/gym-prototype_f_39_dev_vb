import { createId } from '../functions/utils/create_id.js';

export const fakeDataSupport = [
	{
		id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate?',
		date: '20.02.2024',
		input: true
	},
	{
		id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate? Facere, dignissimos voluptatem placeat iure dolore a delectus recusandae tempora distinctio nihil, veritatis quisquam eaque dolores ullam esse non beatae.',
		date: '10.02.2023',
		input: true
	},
	{
		id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate? Facere, dignissimos voluptatem placeat iure dolore a delectus recusandae tempora distinctio nihil, veritatis quisquam eaque dolores ullam esse non beatae.',
		date: '23.01.2024',
		input: false,
		answer:null
	},
	{
		id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate?',
		date: '10.02.2024',
		input: true
	},
	{
		id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate?',
		date: '20.02.2024',
		input: true
	},
	{
		id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate? Facere, dignissimos voluptatem placeat iure dolore a delectus recusandae tempora distinctio nihil, veritatis quisquam eaque dolores ullam esse non beatae.',
		date: '20.02.2024',
		input: false,
		answer: {
					id: createId(),
		title: 'Тема',
		descript:
			'Текст: Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, cupiditate? Facere, dignissimos voluptatem placeat iure dolore a delectus recusandae tempora distinctio nihil, veritatis quisquam eaque dolores ullam esse non beatae.',
			date: '20.02.2024',
			input: true,
		}
	}
];
