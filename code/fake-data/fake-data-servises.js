import { createId } from '../functions/utils/create_id.js';

export const dataServises = [
	{
		id: createId(),
		img: '../image/placeholder-image.jpg',
		title: 'Назва послуги',
		descript: 'Опис послуги',
		price: 100
	},
	{
		id: createId(),
		img: '../image/services/services_1.jpg',
		title: 'Назва послуги',
		descript:
			'Опис послуги Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 300.5
	},
	{
		id: createId(),
		img: '../image/services/services_2.jpg',
		title: 'Назва послуги',
		descript:
			'Опис послуги Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500
	},
	{
		id: createId(),
		img: '../image/services/services_3.jpg',
		title: 'Назва послуги',
		descript:
			'Опис  послугиLorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 200
	},
	{
		id: createId(),
		img: '../image/services/services_4.jpg',
		title: 'Назва послуги',
		descript:
			'Опис Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500
	},
	{
		id: createId(),
		img: '../image/services/services_5.jpg',
		title: 'Назва послуги',
		descript:
			'Опис Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 400
	},
	{
		id: createId(),
		img: '../image/services/services_6.jpg',
		title: 'Назва послуги',
		descript:
			'Опис Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500
	},
	{
		id: createId(),
		img: '../image/services/services_7.jpg',
		title: 'Назва послуги',
		descript:
			'Опис послуги Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 700
	}
];
