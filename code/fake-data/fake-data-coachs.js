import { createId } from '../functions/utils/create_id.js';

export const fakeDataCoachs = [
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/coach_profile.png',
		specialization: 'Тренер',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'coach@ukr.net'
		},

		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/my_profile1.png',
		specialization: 'Лікар',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'doctor@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/coach_profile.png',
		specialization: 'Майстер',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'Master@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/coach_profile.png',
		specialization: 'Тренер',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'coach@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/my_profile1.png',
		specialization: 'Лікар',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'doctor@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/coach_profile.png',
		specialization: 'Майстер',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'Master@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/coach_profile.png',
		specialization: 'Тренер',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'coach@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/my_profile1.png',
		specialization: 'Лікар',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'doctor@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	},
	{
		id: createId(),
		title: "Ім'я Призвище",
		img: '../image/icons/coach_profile.png',
		specialization: 'Майстер',
		biography:
			'Біографія: Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, enim porro amet numquam quasi laboriosam odio aliquid expedita adipisci provident doloremque dolorem minima, a deserunt omnis fugiat, sit inventore iure?',
		contacts: {
			phone: '8 (067) 333 22 11',
			mail: 'Master@ukr.net'
		},
		recommendations: {
			date: '',
			title: '',
			descript: ''
		},
		training_schedule: [
			{
				id: createId(),
				title: '',
				date_time: '',
				name: '',
				coment: '',
				rating: ''
			}
		]
	}
];
