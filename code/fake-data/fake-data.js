import { createId } from '../functions/utils/create_id.js';
export const dataSeasonTickes = [
	{
		id: createId(),
		img: '../image/gym_1.jpg',
		title: 'Абонемент Фітнес-інтенсив',
		descript:
			'Відвідування фітнес залу, 4 заняття з тренером, 8 самостійних занятть, Консультація дієтолога, Особитий план занять',
		date: '28.02.2024 - 30.03.2024',
		time: 'Час: 20:00',
		price: 2000,
		buy: false,
		terms_use: 'умови використання',
		active: true,
		freeze: false
	},

	{
		id: createId(),
		img: '../image/gym_2.jpg',
		title: 'Абонемент Фітнес',
		descript:
			'Відвідування фітнес залу, 4 заняття з тренером, 8 самостійних занятть, Консультація дієтолога, Особитий план занять',
		date: '28.02.2024 - 30.03.2024',
		time: 'Час: 20:00',
		price: 2000,
		buy: false,
		terms_use: 'умови використання',
		active: true,
		freeze: false
	},
	{
		id: createId(),
		img: '../image/gym_3.jpg',
		title: 'Абонемент Фітнес-інтенсив',
		descript:
			'Відвідування фітнес залу, 4 заняття з тренером, 8 самостійних занятть, Консультація дієтолога, Особитий план занять',
		date: '28.02.2023 - 30.03.2023',
		time: 'Час: 20:00',
		price: 2000,
		buy: false,
		terms_use: 'умови використання',
		active: false,
		freeze: true
	},
	{
		id: createId(),
		img: '../image/placeholder-image.jpg',
		title: 'Абонемент Назва',
		descript: 'що водить в абонемент',
		date: '28.02.2023 - 30.03.2023',
		time: 'час відвідування',
		price: 100,
		buy: false,
		terms_use: 'умови використання',
		active: false,
		freeze: true
	}
];


export const dataSportingGoods = [
	{
		id: createId(),
		img: '../image/placeholder-image.jpg',
		title: 'Назва товару',
		descript: 'Опис товару',
		price: 100,
		category: ''
	},
	{
		id: createId(),
		img: '../image/shop/imag_1.jfif',
		title: 'Назва товару',
		descript:
			'Опис товару Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500.5,
		category: ''
	},
	{
		id: createId(),
		img: '../image/shop/image_2.webp',
		title: 'Назва товару',
		descript:
			'Опис товару Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500,
		category: ''
	},
	{
		id: createId(),
		img: '../image/shop/image_3.webp',
		title: 'Назва товару',
		descript:
			'Опис товару Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500,
		category: ''
	},
	{
		id: createId(),
		img: '../image/shop/image_4.webp',
		title: 'Назва товару',
		descript:
			'Опис товару Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500,
		category: ''
	},
	{
		id: createId(),
		img: '../image/shop/image_5.webp',
		title: 'Назва товару',
		descript:
			'Опис товару Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.Lorem ipsum dolor sit amet consectetur adipisicing elit.',
		price: 500,
		category: ''
	}
];