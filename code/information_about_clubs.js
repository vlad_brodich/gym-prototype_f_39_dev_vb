import { removeClass } from './functions/utils/removeClass.js';
import { dataSeasonTickes } from './fake-data/fake-data.js';
import { createTicket, createTicketDetale } from './functions/function_create.js';
import { getIndex } from './functions/utils/getIndex.js';
import { renderDialog } from './component/render-dialog.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const [...schedule_item] = document.getElementsByClassName('schedule_item');
const [...tabs] = document.getElementsByClassName('tab');
const tickets_list = document.getElementById('tickets_list');

const dialog = document.getElementById('dialog');
const dialog_content = document.getElementById('dialog_content');
const main = document.getElementById('main');

const d = new Date();
schedule_item.forEach((day) => {
  if (day.dataset.day == d.getDay()) {
    day.classList.add('_active')
  }else day.classList.remove('_active');
})

// Рендер панелі керування маштабом.
useScaleControl();

main.addEventListener('click', ev => {
	if (ev.target.dataset.tab) {
		removeClass(tabs);
		ev.target.classList.add('_active');
		if (ev.target.dataset.tab === 'active') {
			renderTicketsList(dataSeasonTickes);
		}
		if (ev.target.dataset.tab === 'history') {
			renderTicketsList(dataSeasonTickes, false);
		}
	}

	if (ev.target.dataset.detale) {
		renderTicketDetale(dataSeasonTickes, ev.target.dataset.detale);
	}

	if (ev.target.dataset.freeze) {
		const index = getIndex(dataSeasonTickes, ev.target.dataset.freeze);
		dataSeasonTickes[index].freeze = true;
		renderDialog('Абонімент заморожено на тиждень');
		renderTicketsList(dataSeasonTickes);
  }
	if (ev.target.id === 'sent_btn') {
    renderDialog('Коментар успішно надіслано');
    document.getElementById('input').value = '';
	}
});

dialog.addEventListener('click', () => {
  dialog_content.innerHTML = '';
  dialog.classList.add('_hide');
})


function renderTicketDetale(data, id) {
  const el = data.find(e => e.id === id)
  if (el) {
    dialog_content.innerHTML = ``
    dialog_content.insertAdjacentHTML('beforeend', createTicketDetale(el));
    dialog.classList.remove('_hide');
  }
}

function renderTicketsList(data,flag) {
  tickets_list.innerHTML = '';
  if (!Array.isArray(data) || !data.length) {
    tickets_list.innerHTML = `<li><h2>Немає абониментів</h2></li>`;
    return
  }
  data.forEach((el, i) => {
    tickets_list.insertAdjacentHTML('beforeend', createTicket(el, flag));
  });
}
renderTicketsList(dataSeasonTickes);
basketCaunt()