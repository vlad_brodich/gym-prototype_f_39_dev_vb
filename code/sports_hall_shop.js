import { dataSportingGoods } from './fake-data/fake-data.js';
import { createItemSportingGoods, createItemSportingGoodsDetale } from './functions/function_create.js';
import { getIndex } from './functions/utils/getIndex.js';
import { renderDialog } from './component/render-dialog.js';
import { targetRoutScren } from './functions/utils/targetRoutScren.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const scren = document.getElementById('scren');
const [...routs] = document.getElementsByClassName('rout_item');
const [...header_navs] = document.getElementsByClassName('header_nav');
const list = document.getElementById('list');
const details = document.getElementById('details');

// Рендер панелі керування маштабом.
useScaleControl();

scren.addEventListener('click', ev => {
  if (ev.target.id === 'home_btn') {
		targetRoutScren(routs, 'id', 'scren_main');
		targetRoutScren(header_navs, 'id', 'nav_main');
  }

  if (ev.target.dataset.id_detale) {
    renderDetails(dataSportingGoods, ev.target.dataset.id_detale);
    targetRoutScren(routs, 'id', 'scren_details');
    targetRoutScren(header_navs, 'id', 'nav_details');
  }

	if (ev.target.dataset.id_bay) {
    bay(ev.target.dataset.id_bay);
    targetRoutScren(routs, 'id', 'scren_main');
    targetRoutScren(header_navs, 'id', 'nav_main');
  }
  
});
// Функція додати в кошик.
function bay(id) {
  const basketState = JSON.parse(localStorage.getItem('basketState'))
  let index = getIndex(dataSportingGoods, id);
  localStorage.setItem('basketState', JSON.stringify([{ ...dataSportingGoods[index] }, ...basketState]));
  renderList(dataSportingGoods);
  renderDialog('Товар успішно додано до кошика');
}

function renderList(data) {
  list.innerHTML = '';
  data.forEach((el) => {
    list.insertAdjacentHTML('beforeend', createItemSportingGoods(el));
  })
  basketCaunt()
}
renderList(dataSportingGoods);

function renderDetails(data, id) {
	details.innerHTML = '';
	const el = data[getIndex(data, id)];
	details.insertAdjacentHTML('beforeend', createItemSportingGoodsDetale(el));
	basketCaunt();
}
