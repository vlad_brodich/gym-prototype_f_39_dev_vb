import { fakeDataSupport } from '../fake-data/fake-data-support .js';

// Додаємо localStorage
// Стан кошику товарів.
if (!localStorage.getItem('basketState')) {
	localStorage.setItem('basketState', JSON.stringify([]));
}
// Стан не прочитаних повідомлень.
localStorage.setItem('unread_messages', "0");
// Стан повідомлень в тех-підтримку.
localStorage.setItem('support', JSON.stringify(fakeDataSupport));

// Додаємо sessionStorage.
// Стан маштабу сторінки.
if (!sessionStorage.getItem('useScale')) {
  const use_scale = getComputedStyle(document.documentElement).getPropertyValue('--use-scale');
	sessionStorage.setItem('useScale', `${use_scale.slice(0, -2)}`);
} else {
  const useScale = sessionStorage.getItem('useScale');
  document.documentElement.style.setProperty('--use-scale', `${useScale}vw`);
}
// Стан авторизації.
if (!sessionStorage.getItem('role')) {
	sessionStorage.setItem('role', 'user');
}
