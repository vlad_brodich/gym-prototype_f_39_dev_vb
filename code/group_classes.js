import Calendar from './component/Calendar.js';
import { createItemGroupClasses } from './functions/function_create.js';
import { renderDialog } from './component/render-dialog.js';
import { targetRoutScren } from './functions/utils/targetRoutScren.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const scren = document.getElementById('scren');
const [...routs] = document.getElementsByClassName('rout_item');
const [...header_navs] = document.getElementsByClassName('header_nav');
const details = document.getElementById('details');
const choose_btn = document.getElementById('choose_btn');

const minDate = new Date(); // Мінімальна дата – поточна дата.
const maxDate = new Date(minDate.getFullYear(), minDate.getMonth() + 1, minDate.getDate()); // Максимальная дата - через 1 місяць.
const c = new Calendar('divCal', minDate, maxDate);
c.showcurr();

// Рендер панелі керування маштабом.
useScaleControl();


scren.addEventListener('click', ev => {
  if (ev.target.id === 'home_btn') {
		targetRoutScren(routs, 'id', 'scren_main');
		targetRoutScren(header_navs, 'id', 'nav_main');
	}

	if (ev.target.dataset.id_detale=== '1') {
		targetRoutScren(routs, 'id', 'scren_details');
		targetRoutScren(header_navs, 'id', 'nav_details');
	}

	if (ev.target.dataset.id_btn) {
		if (ev.target.dataset.id_btn === 'btnPrev') {
			c.previousMonth();
			removeActive();
		}
		if (ev.target.dataset.id_btn === 'btnNext') {
			c.nextMonth();
			removeActive();
		}
	}

	if ((ev.target.dataset.id === 'save')) {
		renderDialog('Вас успішно додано до групи');
		setTimeout(() => {
			location.href = '/';
		},2000)
	}
});

basketCaunt();

function removeActive(){
	const [...pickers] = document.getElementsByTagName('td')
	pickers.forEach((el) => {
		const selectedDate = new Date(el.dataset.id);
		if (el.dataset.id && selectedDate > minDate && selectedDate <= maxDate) {
			el.classList.add('_period');
		} else el.classList.remove('_period');

	el.classList.remove('_active')
})
}
	// При виборі дати.
document.getElementById('divCal')
	.addEventListener('click', event => {
		const selectedDate = new Date(event.target.dataset.id);
		if (event.target.dataset.id && selectedDate > minDate && selectedDate <= maxDate) {
			removeActive()
			event.target.classList.add('_active')
			const { month, week } = c.getMonthAndWeekNumber(selectedDate);
			choose_btn.disabled = false;
			details.innerHTML =	createItemGroupClasses({ month: c.Months[month], week: week });
		}
	});

 	removeActive();