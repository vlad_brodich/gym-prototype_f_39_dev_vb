import { removeClass } from './functions/utils/removeClass.js';
import { createItemSupport } from './functions/function_create.js';
import { createId } from './functions/utils/create_id.js';
import { formatDate } from './functions/utils/formatDate.js';
import { routerSwitch } from './functions/utils/router_switch.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const fakeDataSupport = JSON.parse(localStorage.getItem('support'));
const caunt = JSON.parse(localStorage.getItem('unread_messages'));

const messages = document.getElementById('messages');
const messages_form = document.getElementById('messages_form');
const [...options_btns] = document.getElementsByClassName('options_btn');

const scren_main = document.getElementById('scren_main');
const form = document.getElementById('form')

let title = '';
let descript = '';
let idMessag = '';

// Рендер панелі керування маштабом.
useScaleControl();

if (caunt > 0) {
  removeClass(options_btns);
  document.getElementById('options_btn_input').classList.add('_active');
	renderList(fakeDataSupport, false);
	cleanForm();
	routerSwitch(messages, messages_form);
} else {
  renderList(fakeDataSupport);
}

function cauntUnreadMessages() {
  let caunt = 0
  const fakeDataSupport = JSON.parse(localStorage.getItem('support'));
  fakeDataSupport.forEach((el) => {
    if (!el.input && !el.answer) {
    caunt++
	}
  })
  localStorage.setItem('unread_messages', JSON.stringify(caunt));
}

class Message {
	constructor(title,descript) {
		this.id = createId();
		this.title = title;
    this.descript = descript;
    this.date = formatDate(new Date());
    this.input = true;
	}
};

form.addEventListener('input', (ev) => {
  if (ev.target.id === 'title') {
    title = ev.target.value;
  }
  if (ev.target.id === 'description') {
   descript = ev.target.value
  }
})

const validate = (str) => {
  return str.trim() ? true : false;
}
function cleanForm (){
  document.getElementById('title').value = '';
  document.getElementById('description').value = '';
  title = '';
  descript = '';
  idMessag = '';
}

function submit(idMessag, title, descript) {
  let fakeDataSupport = JSON.parse(localStorage.getItem('support'));
	if (validate(title) && validate(descript)) {

    if (idMessag) {
      const newData = fakeDataSupport.map((el) => {
        if (el.id === idMessag) {
          return { ...el, answer: new Message(title, descript) };
        }else return el
      })
      localStorage.setItem('support', JSON.stringify(newData));
      fakeDataSupport = JSON.parse(localStorage.getItem('support'));
    } else {
      localStorage.setItem('support', JSON.stringify([new Message(title, descript), ...fakeDataSupport]));
      fakeDataSupport = JSON.parse(localStorage.getItem('support'));
    }
		renderList(fakeDataSupport);
		cleanForm();
    routerSwitch(messages, messages_form);
    cauntUnreadMessages();
	}
}
messages.addEventListener('click', (ev) => {
  if (ev.target.dataset.reply) {
    messages_form.classList.remove('_hide');
    messages_form.classList.add('_sent');
    title = 'Відповідь';
    idMessag = ev.target.dataset.reply;
  }
})

scren_main.addEventListener('click', (ev) => {
  const fakeDataSupport = JSON.parse(localStorage.getItem('support'));
  if (ev.target.dataset.id) {
    removeClass(options_btns);
    ev.target.classList.add('_active');
    if (ev.target.dataset.id === 'input') {
      
      renderList(fakeDataSupport, false);
      cleanForm()
      routerSwitch(messages,messages_form)
    }
    if (ev.target.dataset.id === 'all') {
      renderList(fakeDataSupport);
      cleanForm()
      routerSwitch(messages,messages_form)
    }
    if (ev.target.dataset.id === 'write') {
      routerSwitch(messages_form, messages)
      messages_form.classList.remove('_sent');
    }
   
    if (ev.target.dataset.id === 'submit') {
      ev.preventDefault()
      submit(idMessag, title, descript);
		}
  }
})

function renderList(data,flag=true) {
 messages.innerHTML= createItemSupport(data,flag)
}

basketCaunt(); 