// Компонент модального вікна.
export function renderDialog(content, tag = false, close = true, time = 1500) {
  const dialog = document.getElementById('dialog');
  const dialog_content = document.getElementById('dialog_content');

	const el = tag ? `${content}` : `<h2 class="message">${content}</h2>`;
	dialog_content.innerHTML = ``;
	dialog_content.insertAdjacentHTML('beforeend', el);
	dialog.classList.remove('_hide');

	if (close) {
		setTimeout(() => {
			dialog_content.innerHTML = ``;
			dialog.classList.add('_hide');
		}, time);
	} else {
		dialog.addEventListener('click', ev => {
			if (ev.target.id === 'dialog') {
				dialog.classList.add('_hide');
			}
		});
	}
}