// Клас створення календаря.
export default class Calendar {
	constructor(divId, minDate, maxDate) {
		// Сохраняем идентификатор div
		this.divId = divId;
		// Дни недели с понедельника
		this.DaysOfWeek = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Нд'];
		// Месяцы начиная с января
		this.Months = [
			'Січень',
			'Лютий',
			'Березень',
			'Квітень',
			'Травень',
			'Червень',
			'Липень',
			'Серпень',
			'Вересень',
			'Жовтень',
			'Листопад',
			'Грудень'
		];
		// Устанавливаем текущий месяц, год
		const d = new Date();
		this.currMonth = d.getMonth();
		this.currYear = d.getFullYear();
		this.currDay = d.getDate();
		// Минимальная и максимальная дата
		this.minDate = minDate ? new Date(minDate) : new Date(-8640000000000000);
		this.maxDate = maxDate ? new Date(maxDate) : new Date(8640000000000000);
	}

	// Перехід до наступного місяця.
	nextMonth() {
		const nextDate = new Date(this.currYear, this.currMonth + 1, 1);
		if (nextDate <= this.maxDate) {
			this.currMonth = nextDate.getMonth();
			this.currYear = nextDate.getFullYear();
			this.showcurr();
		}
	}

	// Перехід до попереднього місяця.
	previousMonth() {
		const prevDate = new Date(this.currYear, this.currMonth - 1, 1);
		if (prevDate <= this.maxDate) {
			this.currMonth = prevDate.getMonth();
			this.currYear = prevDate.getFullYear();
			this.showcurr();
		}
	}

	// Показати поточний місяць
	showcurr() {
		this.showMonth(this.currYear, this.currMonth);
	}

	// Показати місяць (рік, місяць)
	showMonth(y, m) {
		const d = new Date(),
			// Перший день тижня у вибраному місяці.
			firstDayOfMonth = new Date(y, m, 7).getDay(),
			// Останній день обраного місяця.
			lastDateOfMonth = new Date(y, m + 1, 0).getDate(),
			// Останній день попереднього місяця.
			lastDayOfLastMonth = m == 0 ? new Date(y - 1, 11, 0).getDate() : new Date(y, m, 0).getDate();
		let html = '<table>';
		// Запис обраного місяця та року.
		html += `<thead><tr>`;
		html += `<td colspan="7">${this.Months[m]} ${y}</td>`;
		html += `</tr></thead>`;
		// Заголовок днів тижня.
		html += '<tr class="days">';
		for (let day of this.DaysOfWeek) {
			html += `<td>${day}</td>`;
		}
		html += '</tr>';
		// Записуємо дні.
		let i = 1;
		do {
			let dow = new Date(y, m, i).getDay();
			// Почати новий рядок у понеділок.
			if (dow == 1) {
				html += '<tr>';
			}
			// Якщо перший день тижня не понеділок показати останні дні попереднього місяця.
			else if (i == 1) {
				html += '<tr>';
				let k = lastDayOfLastMonth - firstDayOfMonth + 1;
				for (let j = 0; j < firstDayOfMonth; j++) {
					html += `<td class="not-current">${k}</td>`;
					k++;
				}
			}
			// Записуємо поточний день у цикл.
			const chk = new Date();
			const chkY = chk.getFullYear();
			const chkM = chk.getMonth();
			if (chkY == this.currYear && chkM == this.currMonth && i == this.currDay) {
				html += `<td class="today" data-id="${this.idDate(this.currYear,this.currMonth,i)}">${i}</td>`;
			} else {
				html += `<td class="normal" data-id="${this.idDate(this.currYear,this.currMonth,i)}">${i}</td>`;
			}
			// Закрити рядок у неділю.
			if (dow == 0) {
				html += '</tr>';
			}
			// Якщо останній день місяця не неділя, показати перші дні наступного місяця.
			else if (i == lastDateOfMonth) {
				var k = 1;
				for (dow; dow < 7; dow++) {
					html += `<td class="not-current">${k}</td>`;
					k++;
				}
			}
			i++;
		} while (i <= lastDateOfMonth);
		// Кінець таблиці.
		html += '</table>';
		// Записуємо HTML в div
		document.getElementById(this.divId).innerHTML = html;
	}

	// Отримати місяць та номер тижня для вибраної дати.
	getMonthAndWeekNumber(selectedDate) {
		const date = new Date(selectedDate);
		const month = date.getMonth();
		const year = date.getFullYear();

		// Знайти перший день місяця.
		const firstDayOfMonth = new Date(year, month, 1).getDay();
		// Знайти перший понеділок у місяці.
		const firstMonday = 1 + ((7 - firstDayOfMonth) % 7);
		// Знайти номер тижня, в якому знаходиться обрана дата.
		let weekNumber = Math.ceil((date.getDate() - firstMonday) / 7) + 1;

		// Якщо перший понеділок у місяці відбувається в наступному місяці.
		if (firstMonday > date.getDate()) {
			weekNumber = 1;
		}

		return { month: month, week: weekNumber };
  }

  idDate(r,m,d) {
	  return `${r}-${m+1}-${d}`
  }

}