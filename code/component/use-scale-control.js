// Компонент управління маштабом відображення.
export function useScaleControl() {
  const use_scale = getComputedStyle(document.documentElement)
    .getPropertyValue('--use-scale');
  
  document.getElementById('use_scale_control')
    .innerHTML = `
      <div class="scale_max">
        <label for="">Маштаб: <span id="scale_result">
          ${use_scale.slice(0, -2) * 100}
        </span>%</label>
        <label for="">Макс.</label>
      </div>
      <input type="range" min="0.4" max="2" step="0.1" id="range_scale" value="${use_scale.slice(0, -2)}">
      <label for="">Мін.</label>`;
  
  document.getElementById('range_scale').addEventListener('input', ev => {
    document.documentElement.style.setProperty('--use-scale', `${ev.target.value}vw`);
    document.getElementById('scale_result').innerHTML = `${(ev.target.value * 100).toFixed()}`;
    sessionStorage.setItem('useScale', `${ev.target.value}`);
  });
}

