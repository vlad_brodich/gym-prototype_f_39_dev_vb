import { getRandomInt } from '../functions/utils/getRandomInt.js';
// Компонент кількість людей в клубі. Як що не робочій час- вивіска зачинено.
export function createContentCauntPeople() {
	const d = new Date();
	const h = d.getHours();
	const m = d.getMinutes();
	const time = `${h}:${m < 10 ? `0${m}` : m}`;
	let content = '';

	if (h >= 8 && h < 22) {
		content = `
		<div class="people_img">
			<img src="./image/icons/fitness-exercises.png" alt="fitness">
		</div>
		<p>${time}</p>
		<p>в клубі ${getRandomInt(7, 35)} відвідувачив</p>
	`;
	} else {
		content = `
		<div class="people_img closed">
			<img src="./image/icons/closed.png" alt="fitness">
		</div>
		<p>${time}</p>
		<p>Клуб зачинений</p>
	`;
	}
	return content;
}
