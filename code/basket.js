import { thousandSeparator } from './functions/utils/thousandSeparator.js';
import { routerSwitch } from './functions/utils/router_switch.js';
import { renderDialog } from './component/render-dialog.js';
import { createPayService, createConfirmPayment } from './functions/function_create.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const scren = document.getElementById('scren');
const [...header_navs] = document.getElementsByClassName('header_nav');
const [...total_price] = document.getElementsByClassName('total_price');

const wrapper = document.getElementById('wrapper');
const payment = document.getElementById('payment');
// Змінна емітації помилки.
let error = true;
let totalPrace = 0;

// Рендер панелі керування маштабом.
useScaleControl();

scren.addEventListener('click', (ev) => {
	if (ev.target.id === 'home_btn') {
		routerSwitch(wrapper, payment);
		header_navs.forEach(el => {
			el.dataset.id === 'nav_main' ? el.classList.remove('_hide') : el.classList.add('_hide');
		});
	}

	if (ev.target.dataset.id === 'go_payment') {
		routerSwitch(payment, wrapper);
		renderPay();
		header_navs.forEach(el => {
			el.dataset.id === 'nav_pay' ? el.classList.remove('_hide') : el.classList.add('_hide');
		});
	}

	if (ev.target.dataset.pay_service) {
		renderPay(ev.target.dataset.pay_service);
	}
})
// Функція оплати
function submit() {
	if (error) {
		renderDialog(`Помилка`);
		renderPay();
		error = false;
	} else {
		renderDialog(`Оплата успішна`);
		error = true;
		localStorage.setItem('basketState', JSON.stringify([]));
		renderList()
		basketCaunt()
		routerSwitch(wrapper, payment);
		header_navs.forEach(el => {
			el.dataset.id === 'nav_main' ? el.classList.remove('_hide') : el.classList.add('_hide');
		});
	}
}
// Функція створення сторінки кошика
function createList(data) {
	const li = data.map(el => {
		totalPrace += el.price;
		return `
		<li class="list_item">
			<div class="item_img">
				<img src="${el.img}" alt="${el.title}">
			</div>
			<h2 class="item_title">${el.title}</h2>
			<p class="item_price">Ціна: <span>${thousandSeparator(el.price)}</span> грн.</p>
		</li>`;
	});
	total_price.forEach((el) => {
		el.innerHTML = `Загальна сума: <span>${thousandSeparator(totalPrace)}</span> грн.`;
	})

	return `      
	<ul class="list" id="list">
	${li.join(' ')}
	</ul>
	<div class="box_pay">
		<p class="total_price" id="total_price">Загальна сума: <span>${thousandSeparator(totalPrace)}</span> грн.</p>
		<button class="btn-submit" type="button" data-id="go_payment">Перейти до сплати</button>
	</div>`;
}
// Функція рендер сторінки кошика
function renderList() {
	const data = JSON.parse(localStorage.getItem('basketState'));
	if (data.length) {
		wrapper.innerHTML = '';
		wrapper.insertAdjacentHTML('beforeend', createList(data));
		basketCaunt();
	} else {
		wrapper.innerHTML = `
	 <div class="wrapper _empty" >
			<h2>Кошик порожній.</h2>
			<a class="home-btn" href="/">❮ На головну</a>
		</div> `;
	}
}
// Функція рендер сторінки олати.
function renderPay(img) {
	if (img) {
		payment.innerHTML = createConfirmPayment(img,totalPrace);

		document.getElementById('checkbox').addEventListener('change', ev => {
			document.getElementById('btn-submit').disabled = !ev.target.checked;
		});

		document.getElementById('btn-submit').addEventListener('click', () => {
			submit()
		})
	} else {
		payment.innerHTML = createPayService();
	}
}

renderList();
renderPay();