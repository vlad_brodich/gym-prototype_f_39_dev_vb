import { dataServises } from './fake-data/fake-data-servises.js';
import { createItemSportingGoods, createItemSportingGoodsDetale } from './functions/function_create.js';
import { getIndex } from './functions/utils/getIndex.js';
import { renderDialog } from './component/render-dialog.js';
import { targetRoutScren } from './functions/utils/targetRoutScren.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const list = document.getElementById('list');
const scren = document.getElementById('scren');
const [...routs] = document.getElementsByClassName('rout_item');
const [...header_navs] = document.getElementsByClassName('header_nav');

// Рендер панелі керування маштабом.
useScaleControl();

scren.addEventListener('click', ev => {
  if (ev.target.id === 'home_btn') {
		targetRoutScren(routs, 'id', 'scren_main');
		targetRoutScren(header_navs, 'id', 'nav_main');
	}
	if (ev.target.dataset.id_detale) {
		renderDetails(dataServises, ev.target.dataset.id_detale);
		targetRoutScren(routs, 'id', 'scren_details');
		targetRoutScren(header_navs, 'id', 'nav_details');
	}

	if (ev.target.dataset.id_bay) {
		bay(ev.target.dataset.id_bay);
    targetRoutScren(routs, 'id', 'scren_main');
		targetRoutScren(header_navs, 'id', 'nav_main');
	}

});

// Функція придбати.
function bay(id) {
	const basketState = JSON.parse(localStorage.getItem('basketState'));
	 let index = getIndex(dataServises, id);
	localStorage.setItem('basketState', JSON.stringify([{ ...dataServises[index] }, ...basketState]));
	renderDialog('Послугу успішно додано до кошика');
	renderList(dataServises);
}
// Рендер основної сторінки.
function renderList(data) {
	list.innerHTML = '';
	data.forEach(el => {
		list.insertAdjacentHTML('beforeend', createItemSportingGoods(el));
	});
	basketCaunt();
}
renderList(dataServises);
// Рендер сторінки додаткової інформації.
function renderDetails(data, id) {
	details.innerHTML = '';
	const el = data[getIndex(data, id)];
	details.insertAdjacentHTML('beforeend', createItemSportingGoodsDetale(el));
	basketCaunt();
}
