import { createDetails } from './functions/function_create.js';
import { fakeDataSupport } from './fake-data/fake-data-support .js';
import { targetRoutScren } from './functions/utils/targetRoutScren.js';
import { basketCaunt } from './component/basketCaunt.js';
import { createContentCauntPeople } from './component/createContentCauntPeople.js';
import { renderDialog } from './component/render-dialog.js';
import { useScaleControl } from './component/use-scale-control.js';

const [...header_navs] = document.getElementsByClassName('header_nav');
const [...slider_items] = document.getElementsByClassName('slider_item');
const [...control_btns] = document.getElementsByClassName('slider_control_btn');

const sliderBox = document.getElementById('slider_items');
const details = document.getElementById('details');
const scren = document.getElementById('scren');
const [...routs] = document.getElementsByClassName('rout_item');
const qr_code = document.getElementById('qr_code');

// Рендер панелі керування маштабом.
useScaleControl();

function cauntUnreadMessages() {
	let caunt = 0;
	const fakeDataSupport = JSON.parse(localStorage.getItem('support'));
	fakeDataSupport.forEach(el => {
		if (!el.input && !el.answer) {
			caunt++;
		}
	});
	localStorage.setItem('unread_messages', JSON.stringify(caunt));
}
cauntUnreadMessages();

// Контент форми авторизації.
const form = `
	<form class="menu_content_form">
		<h2>Авторизуватися як тренер.</h2>
		<input type="text" placeholder="Логін" required minlength="2">
		<input type="password" placeholder="Пароль" minlength="6" required>
		<button type="submit" data-id="submit">Увійти</button>
	</form>
`;

scren.addEventListener('click', (ev) => {
  if (ev.target.id === 'home_btn') {
		targetRoutScren(routs, 'id', 'scren_main');
		targetRoutScren(header_navs, 'id', 'nav_main');
	}

  if (ev.target.dataset.id === 'tech_banner') {
		renderDialog(`Докладніше про технічний банер.  Важлива інформація. Попередження. Тощо...`, false, false);
	}

	if (ev.target.dataset.src) {
		renderSliderDetale(ev.target.src);
		targetRoutScren(routs, 'id', 'scren_details');
		targetRoutScren(header_navs, 'id', 'nav_details');
	}

	if (ev.target.dataset.id_control) {
		index = +ev.target.dataset.id_control;
		slider();
	}

	if (ev.target.dataset.id === 'btn_coach_profile') {
		const role = sessionStorage.getItem('role');
		if (role === 'coach') {
			location.href = './pages/coach_office.html';
		} else {
			renderDialog(form, true, false);
		}
	}

	if (ev.target.dataset.id === 'btn_menu') {
		document.getElementById('menu').classList.toggle('_hide');
	}

	if (ev.target.dataset.id === 'btn_authorize') {
		document.getElementById('menu_content_form').classList.remove('_hide')
		document.getElementById('btn_authorize').classList.add('_hide');
	}

	if (ev.target.dataset.id === 'submit') {
		ev.preventDefault()
		sessionStorage.setItem('role', 'coach');
		renderBtnCoachProfile();
		document.getElementById('dialog').classList.add('_hide');
		document.getElementById('menu').classList.add('_hide');
		document.getElementById('menu_content_form').classList.add('_hide');
		location.href = './pages/coach_office.html';
	}
})

qr_code.addEventListener('click', () => {
	qr_code.classList.toggle('_active')
})

sliderBox.addEventListener('mouseover', () => {
	stopLoop();
});

sliderBox.addEventListener('mouseout', () => {
	startLoop();
});

let index = slider_items.length - 1;

const slider = () => {
	slider_items.forEach((el, i) => {
		if (index !== i) {
			slider_items[i].classList.add('_hide');
			control_btns[i].classList.remove('_active');
		} else {
			slider_items[i].classList.remove('_hide');
			control_btns[i].classList.add('_active');
		}
	});
};

// Функція самостійного руху слайдів.  
function moveSlider() {
    if (index == 0) {
        index = slider_items.length - 1;
    } else index--;
    slider(); 
	stopLoop();
	startLoop();
};

let timeoutId;
let requestId;
// Функція старт слайд.  
export function startLoop() {
	timeoutId = setTimeout(() => (requestId = requestAnimationFrame(moveSlider)), 5000);
};

// Функція стоп сдайд.  
export function stopLoop(){
	cancelAnimationFrame(requestId);
	timeoutId = clearTimeout(timeoutId);
};

function renderSliderDetale(src) {
	details.innerHTML = createDetails(src);
}

function renderCauntPeople() {
	document.getElementById('people').innerHTML = createContentCauntPeople();
}

function renderBtnCoachProfile() {
	const btn = document.getElementById('btn_coach_profile');
	const role = sessionStorage.getItem('role')
	if (role !== 'coach') {
		document.getElementById('btn_authorize').classList.remove('_hide');
		btn.innerHTML = `<img data-id="btn_coach_profile" src="./image/icons/block-user.png" alt="my_profile">
    <span data-id="btn_coach_profile">Кабінет тренера</span>`;
		document.getElementById('menu_coach_profile').classList.add('_hide');
	} else {
		document.getElementById('btn_authorize').classList.add('_hide');
		btn.innerHTML = `<img data-id="btn_coach_profile" src="./image/icons/coach_profile.png" alt="my_profile">
    <span data-id="btn_coach_profile">Кабінет тренера</span>`;
		document.getElementById('menu_coach_profile').classList.remove('_hide');
	}
}

renderBtnCoachProfile();
renderCauntPeople();
startLoop();
basketCaunt();