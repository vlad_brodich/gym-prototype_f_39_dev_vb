import { renderDialog } from './component/render-dialog.js';
import { fakeDataCoachs } from './fake-data/fake-data-coachs.js';
import { getRandomInt } from './functions/utils/getRandomInt.js';
import { createCoachProfile } from './functions/function_create.js';
import { targetRoutScren } from './functions/utils/targetRoutScren.js';
import { useScaleControl } from './component/use-scale-control.js';

const scren = document.getElementById('scren');
const [...profiles] = document.getElementsByClassName('profile');
const [...header_navs] = document.getElementsByClassName('header_nav');
const sent_caunt = document.getElementById('sent_caunt');

const caunt = JSON.parse(localStorage.getItem('unread_messages'));
sent_caunt.innerHTML = caunt > 0 ? caunt : '';

const form = `
  <form class="form" id="form">
    <label class="label" for="description">Текст повідомлення.</label>
    <textarea name="" id="description" minlength="3" cols="30" rows="3" ></textarea>
    <button class="btn-submit"  type="button" data-id="submit">
      Відправити
      <img src="../image/icons/sent.png" alt="sent" data-id="submit">
      </button>
  </form>`;
  
const training_plan = `
  <form class="form" id="form">
    <label class="label" for="description">Додати план тренувань.</label>
    <textarea name="" id="description" minlength="3" cols="30" rows="3" ></textarea>
    <button class="btn-submit"  type="button" data-id="submit">
      Додати
    </button>
  </form>`;

const recommend = `
  <form class="form" id="form">
    <label class="label" for="description">Харчування.</label>
    <textarea name="" id="description" minlength="3" cols="30" rows="3" ></textarea>
    <label class="label" for="description1">Тренування.</label>
    <textarea name="" id="description1" minlength="3" cols="30" rows="3" ></textarea>
    <label class="label" for="description2">Інше...</label>
    <textarea name="" id="description2" minlength="3" cols="30" rows="3" ></textarea>
    <button class="btn-submit"  type="button" data-id="submit">
      Додати
    </button>
  </form>`;

const verification = `  
  <div class="verification">          
    <a class="contact" href="./my_office.html">
      <img src="../image/icons/my_profile_2.png" alt="profile">
      <span>Профіль клієнта</span>
    </a>

    <div class="contact" data-id="sent_dev">
      <img src="../image/icons/mail.png" alt="support">
       Написати кліенту
    </div>

    <div class="contact" data-id="sent_dev">
      Запланувати заняття.
    </div>
      <form class="form" id="form">
        <label class="label" for="date">Запланувати заняття.</label>
        <label class="label" for="date">Дата та час заняття.</label>
        <input type="datetime-local" id="date">

        <label class="label" for="description2">План заняття.</label>
        <textarea name="" id="description2" minlength="3" cols="30" rows="3" ></textarea>
        <button class="btn-submit"  type="button" data-id="submit">
      Додати
    </button>
  </form>
  </div>`;

const change_time = `
  <form class="form" id="form">
    <label class="label" for="date">Змінити час заняття.</label>
    <input type="datetime-local" id="date">
    <button class="btn-submit"  type="button" data-id="submit">
      Зберегти
    </button>
  </form>`;

const change_plan = `
  <form class="form">
    <label class="label" for="description">Змінити план тренувань.</label>
    <textarea name="" id="description" minlength="3" cols="30" rows="3" ></textarea>
    <button class="btn-submit"  type="button" data-id="submit">
      Зберегти
    </button>
  </form>`;  
  
// Рендер панелі керування маштабом.
useScaleControl();

function renderCoachProfile() {
  document.getElementById('profile_detale').innerHTML = createCoachProfile(
		fakeDataCoachs[getRandomInt(0, fakeDataCoachs.length-1)]
	);
}
renderCoachProfile();

document.getElementById('my_profile_btn').addEventListener('click', () => {
  targetRoutScren(profiles, 'profile', 'detale');
  targetRoutScren(header_navs, 'id', 'nav_my_profile');
});

scren.addEventListener('click', ev => {
  if (ev.target.id === 'home_btn') {
    targetRoutScren(profiles, 'profile', 'main');
    targetRoutScren(header_navs, 'id', 'nav_main');
	}

  if (ev.target.dataset.id === 'my_customers_btn') {
    targetRoutScren(profiles, 'profile', 'customers');
    targetRoutScren(header_navs, 'id', 'nav_customers');
	}

  if (ev.target.dataset.id === 'schedule_btn') {
    targetRoutScren(profiles, 'profile', 'schedule');
    targetRoutScren(header_navs, 'id', 'nav_schedule');
	}
	if (ev.target.dataset.training) {
		renderDialog(training_plan, false, false);
	}

	if (ev.target.dataset.recommend) {
		renderDialog(recommend, false, false);
	}

	if (ev.target.dataset.verification) {
		renderDialog(verification, false, false);
	}

	if (ev.target.dataset.change_time) {
		renderDialog(change_time, false, false);
  }
  
	if (ev.target.dataset.change_plan) {
		renderDialog(change_plan, false, false);
  }

  if (ev.target.dataset.delite) {
    document.getElementById(ev.target.dataset.delite)
      .classList.add('_hide')
  }
  
	if (ev.target.dataset.id === 'sent_dev') {
		renderDialog(form, true, false);
	}
	if (ev.target.dataset.id === 'submit') {
		document.getElementById('dialog').classList.add('_hide');
		renderDialog('Успішно надіслано', false);
	}
});

// Редагування фото. submit
document.getElementById('btn_edit').addEventListener('click', () => {
	document.getElementById('profile_img').classList.toggle('_edit');
});