import { removeClass } from './functions/utils/removeClass.js';
import { routerSwitch } from './functions/utils/router_switch.js';
import { renderDialog } from './component/render-dialog.js';
import { fakeDataCoachs } from './fake-data/fake-data-coachs.js';
import { createCoachItem, createCoachDetaleItem } from './functions/function_create.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const dialog = document.getElementById('dialog');
const [...options_btns] = document.getElementsByClassName('options_btn');
const scren = document.getElementById('scren');
const coachs = document.getElementById('coachs');
const coachs_detale = document.getElementById('coachs_detale');
const [...header_navs] = document.getElementsByClassName('header_nav');
const coach_list = document.getElementById('coach_list');

const recommendations = `
  <div class="my_message">
    <p class="message_text">дата</p>
    <h2>Ім'я Призвище</h2>
    <p class="message_text">Рекомендації від тренера</p>
  </div>
`;

const schedule = `
  <div class="my_message">
    <div class="message_img">
      <img src="../image/icons/rating.png" alt="rating">
    </div>
    <p class="message_text">дата</p>
    <h2>Тренер</h2>
    <p class="message_text">Побажання тренера (коментар)</p>
  </div>
`;

// Рендер панелі керування маштабом.
useScaleControl();

scren.addEventListener('click', (ev) => {
  if (ev.target.dataset.id === 'coach') {
    removeClass(options_btns);
    ev.target.classList.add('_active');
    renderList('Тренер');
  }

  if (ev.target.dataset.id === 'doctor') {
    removeClass(options_btns);
    ev.target.classList.add('_active');
    renderList('Лікар');
  }

  if (ev.target.dataset.id === 'master') {
    removeClass(options_btns);
    ev.target.classList.add('_active');
    renderList('Майстер');
  }

  if (ev.target.dataset.detale) {
    header_navs.forEach(el => {
      el.dataset.id === 'detale' ? el.classList.remove('_hide') : el.classList.add('_hide');
    });
    routerSwitch(coachs_detale, coachs)
    renderCoachDetale(ev.target.dataset.detale);
  }

  if (ev.target.id === 'home_btn') {
    header_navs.forEach(el => {
      el.dataset.id === 'main' ? el.classList.remove('_hide') : el.classList.add('_hide');
    });
    routerSwitch(coachs,coachs_detale)
  }
  if (ev.target.dataset.recommendations) {
    renderDialog(recommendations, true, false);
  }

  if (ev.target.dataset.schedule) {
    renderDialog(schedule, true, false);
  }

  if (ev.target.dataset.id === 'submit') {
    renderDialog('Заявка відправлена тренеру для підтвердження.', false, true);
    header_navs.forEach(el => {
      el.dataset.id === 'main' ? el.classList.remove('_hide') : el.classList.add('_hide');
    });
    routerSwitch(coachs, coachs_detale);

  }
})

function filterData(data,filter) {
  return data.filter((el) => { return el.specialization === filter });
}

function getCoach(data, id) {
	return data.find(el => {
		return el.id === id;
	});
}

function renderList(filter = '') {
  coach_list.innerHTML=''
  if (filter && filterData(fakeDataCoachs, filter)) {
    filterData(fakeDataCoachs, filter).forEach(el => {
      coach_list.insertAdjacentHTML('beforeend', createCoachItem(el));
    })
  } else {
    fakeDataCoachs.forEach(el => {
      coach_list.insertAdjacentHTML('beforeend', createCoachItem(el));
    });
  }
}

function renderCoachDetale(id) {
	coachs_detale.innerHTML = '';
  if (id && getCoach(fakeDataCoachs, id)) {
  coachs_detale.innerHTML = createCoachDetaleItem(getCoach(fakeDataCoachs, id));
	}
}
renderList();
basketCaunt()