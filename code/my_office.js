import { renderDialog } from './component/render-dialog.js';
import { basketCaunt } from './component/basketCaunt.js';
import { useScaleControl } from './component/use-scale-control.js';

const dialog = document.getElementById('dialog');
const sent_caunt = document.getElementById('sent_caunt');

const caunt = JSON.parse(localStorage.getItem('unread_messages'));
sent_caunt.innerHTML = caunt > 0 ? caunt : '';

const services = `
  <h2 class="may_content">
    Інформація по додатковим послугам які куплені та не використані.
  </h2>
`;

const goods = `
  <h2 class="may_content">
    Інформація по товарам послугам які куплені та очікують отримання.
  </h2>
`;

const history = `
  <h2 class="may_content">
    Історія покупок.
  </h2>
`;

// Рендер панелі керування маштабом.
useScaleControl();

document.getElementById('scren').addEventListener('click', (ev) => {
  if (ev.target.dataset.id === 'btn_services') {
		renderDialog(services, true, false);
  }
  
  if (ev.target.dataset.id === 'btn_goods') {
		renderDialog(goods, true, false);
  }
  
  if (ev.target.dataset.id === 'btn_history') {
		renderDialog(history, true, false);
	}
  
})

basketCaunt()